﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tank2 : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public ParticleSystem fire;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            fire.Play();
        }
        else
        {
            fire.Stop();
        }
    }
}
