﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class TankEnemy : MonoBehaviour
{


    [SerializeField] private float _movementSpeed;
    [SerializeField] private Transform _player;
    [SerializeField] private GameObject _Bullet;
    [SerializeField] private Transform _SpawnPoint;
    [SerializeField] private int _startHp;
    [SerializeField] private int _hp;
    [SerializeField] private GameObject _explosion;

    private bool _playerInRange = false;
    private float _shootingTimer;

    private void Start()
    {
        _hp = _startHp;
    }

    private float AngleBetweenVector2(Vector2 vec1, Vector2 vec2)
    {
        Vector2 diference = vec2 - vec1;
        float sign = (vec2.y < vec1.y) ? -1.0f : 1.0f;
        return Vector2.Angle(Vector2.right, diference) * sign;
    }

    public void FollowThePlayer()
    {
        if (RotateToPlayer())
        {
            transform.position -= transform.up * _movementSpeed;
        }
    }

    public bool RotateToPlayer()
    {
        float angle = AngleBetweenVector2(new Vector2(transform.position.x, transform.position.y)
            , new Vector2(_player.transform.position.x, _player.transform.position.y));
        Quaternion FinalRotation = Quaternion.Euler(0, 0, angle + 90);
        transform.rotation = Quaternion.Lerp(transform.rotation, FinalRotation, 0.03f);
        if (transform.rotation.eulerAngles.z < FinalRotation.eulerAngles.z + 1f && transform.rotation.eulerAngles.z > FinalRotation.eulerAngles.z - 1f)
        {
            return true;
        }
        return false;
    }

    public void Shoot()
    {
        if (RotateToPlayer())
        {
            _shootingTimer += Time.deltaTime;
            if (_shootingTimer > 3)
            {
                Instantiate(_Bullet, _SpawnPoint);
                _shootingTimer = 0;
            }
        }

    }

    void Update()
    {
        if (_playerInRange)
        {
            Shoot();
            //shoo
        }
        else
        {
            //follow the player
            FollowThePlayer();
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("enter");
        if (other.gameObject.CompareTag("Player"))
        {
            _playerInRange = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        Debug.Log("Exit");
        if (other.gameObject.CompareTag("Player"))
        {
            Debug.Log("exit");
            _playerInRange = false;

        }
    }

    public void Death()
    {
        if (_hp <= 0)
        {
            _explosion.SetActive(true);
            _explosion.transform.parent = null;
            Destroy(this.gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("PlayerBullet"))
        {
            Debug.Log("hit");
            _hp -= other.gameObject.GetComponent<BulletScript>().Damage;
            Death();
        }
    }
}
