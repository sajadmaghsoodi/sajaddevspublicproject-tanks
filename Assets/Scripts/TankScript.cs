﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankScript : MonoBehaviour
{
    [SerializeField] private float _movementSpeed;
    [SerializeField] private Rigidbody2D _rigidbody;
    [SerializeField] private GameObject _bullet;
    [SerializeField] private CameraScript _cameraScript;
    [SerializeField] private Transform _spawnPoint;
    [SerializeField] private int _hp;
    [SerializeField] private int _startHp;

    private bool _active = true;


    void Start()
    {
        _hp = _startHp;
    }

    void Update()
    {
        if (_active)
        {
            Movement();
            Gun();
        }
    }

    public void Movement()
    {
        if (Input.GetKey(KeyCode.W))
        {
            _rigidbody.velocity = new Vector2(0, 0);
            transform.position = new Vector3(transform.position.x, transform.position.y + _movementSpeed);
            transform.rotation = Quaternion.Euler(0, 0, 180);
        }
        else if (Input.GetKey(KeyCode.S))
        {
            _rigidbody.velocity = new Vector2(0, 0);
            transform.position = new Vector3(transform.position.x, transform.position.y - _movementSpeed);
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }
        else if (Input.GetKey(KeyCode.A))
        {
            _rigidbody.velocity = new Vector2(0, 0);
            transform.position = new Vector3(transform.position.x - _movementSpeed, transform.position.y);
            transform.rotation = Quaternion.Euler(0, 0, -90);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            _rigidbody.velocity = new Vector2(0, 0);
            transform.position = new Vector3(transform.position.x + _movementSpeed, transform.position.y);
            transform.rotation = Quaternion.Euler(0, 0, 90);
        }
    }

    public void Gun()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Instantiate(_bullet, _spawnPoint);
            _cameraScript.Shake();
        }
    }

    public void Death()
    {
        if (_hp <= 0)
        {
            //player died
            _active = false;
        }
    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("EnemyBullet"))
        {
            _hp -= other.gameObject.GetComponent<BulletScript>().Damage;
            Death();
        }
    }
}
