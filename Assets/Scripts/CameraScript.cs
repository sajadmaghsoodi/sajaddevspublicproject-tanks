﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    [SerializeField] private GameObject _player;
    [SerializeField] private float _shakeInt;
    [SerializeField] private float _shakeDis;
    [SerializeField] private Vector3 _shakePos;
    [SerializeField] private Vector3 _orginPos;
    [SerializeField] private AnimationCurve _shakeCurve;

    private float currentTime;

    public void Shake()
    {
        currentTime = Time.time;
    }

    void Start()
    {
        _orginPos = transform.position;
        currentTime = -10;
    }

    void Update()
    {
        var Xpos = (Time.time) * _shakeInt + 10;
        var Ypos = (Time.time) * _shakeInt + 100;
        _shakePos = new Vector3((Mathf.PerlinNoise(Xpos, 1) - 0.5f) * _shakeDis,
            (Mathf.PerlinNoise(Ypos, 1) - 0.5f) * _shakeDis, 0) * _shakeCurve.Evaluate(Time.time - currentTime);
        transform.position = _orginPos + _shakePos;
        _orginPos = Vector3.Lerp(transform.position, new Vector3(_player.transform.position.x, _player.transform.position.y,
            transform.position.z), 0.05f);
    }
}
