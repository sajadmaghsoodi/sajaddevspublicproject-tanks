﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    public int Damage;

    [SerializeField] private CameraScript _camera;
    [SerializeField] private float _force;
    [SerializeField] private GameObject _explosion;

    void Start()
    {
        _camera = GameObject.Find("Main Camera").GetComponent<CameraScript>();
        GetComponent<Rigidbody2D>().AddForce(transform.up * _force);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        _camera.Shake();
        _explosion.SetActive(true);
        _explosion.transform.parent = null;
        Destroy(gameObject);
    }
}
